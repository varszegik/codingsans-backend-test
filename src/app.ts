import express, { Request, Response } from 'express';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import assert from 'assert';
import axios from 'axios';
import morgan from 'morgan';
import { MongoClient } from 'mongodb';
import { config } from './config';

export const startApp = async function(): Promise<void> {
  const app = express();
  app.use(morgan('combined'));
  app.use(express.json());
  const users: { id: string; username: string; password: string }[] = [];

  const uri = config.DB_URL;
  const cl = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });
  await cl.connect().then(function(client) {
    assert.notEqual(null, client);
    const db = client.db('test');
    const collection = db.collection('users');
    collection.find({}).toArray(function(err: Error, docs: { id: string; username: string; password: string }[]) {
      assert.equal(err, null);
      users.push(...docs);
      client.close().catch(() => console.log('Db connection could not be closed'));
    });
  });

  app.listen(3000, () => {
    console.log('App listening on port 3000');
  });

  app.post('/login', async (req: Request, res: Response) => {
    const user = users.find(user => user.username === req.body.username);
    if (user) {
      if (await bcrypt.compare(req.body.password, user.password)) {
        const token = jwt.sign(user, config.JTW_SECRET);
        return res.send(token);
      } else {
        return res.status(401).send('Invalid password');
      }
    }
    return res.status(401).send('Invalid username');
  });

  app.get('/breweries', async (req: Request, res: Response) => {
    const token = req.headers['authorization'];
    if (token) {
      try {
        jwt.verify(token, config.JTW_SECRET);
        try {
          const params = new URLSearchParams();
          params.append('query', req.query.query.toString());
          const response = await axios.get('https://api.openbrewerydb.org/breweries/search', { params });
          res.json(response.data);
        } catch {
          const response = await axios.get('https://api.openbrewerydb.org/breweries');
          res.json(response.data);
        }
      } catch {
        res.status(401).send('Authentication failed');
      }
    } else {
      res.status(401).send('No token was provided');
    }
  });
};
