import dotenv from 'dotenv';

dotenv.config();

export const config = {
  JTW_SECRET: process.env.JTW_SECRET || '',
  DB_URL: process.env.DB_URL || '',
};
